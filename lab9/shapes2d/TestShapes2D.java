package shapes2d;

public class TestShapes2D {
    public static void main(String[] args){

        Circle c1 = new Circle(5);
        c1 = new Circle(6);  // new object

        Circle c2 = new Circle(6);

        String str = new String("hello"); // immutable
        String str2 = "hola";

        // str == str2   compares object not content
        System.out.println("str.equals(str2) : "+str.equals(str2)); //correct comparison



        System.out.println("c1 == c2 : "+ (c1 == c2)); // false

        System.out.println("c1.equals(c2) : "+ c1.equals(c2));
    }
}
