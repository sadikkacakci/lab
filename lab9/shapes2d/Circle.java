package shapes2d;

public class Circle extends Object{
    protected final int radius;


    public Circle(int radius) {
        this.radius = radius;
    }

    public double area(){
        return Math.PI * radius * radius;
    }

    public String toString(){
        return "radius = "+ radius;
    }

    @Override
    public boolean equals(Object obj){
        if(this == obj){
            return true;
        }
        if(obj instanceof Circle){
            Circle c = (Circle) obj;   //downcasting
            return radius == c.radius;
        }
        return false;


    }
}
