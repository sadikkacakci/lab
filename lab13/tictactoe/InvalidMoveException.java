package tictactoe;

public class InvalidMoveException extends Exception{


    public InvalidMoveException(String message){
        super(message);
    }

    @Override
    public String getMessage() {
        return super.getMessage();
    }
}